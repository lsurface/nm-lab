#include<stdio.h>
#include<math.h>
#define pi 3.14159

//Considering f(y) to be sin(pi*y)

float formula(float u1, float u2, float u3, float u4){

    return (u1+u2+u3+u4)/4;
}

int main(){

    float u[5][5];

    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            //printf("Enter value for u[%d][%d]",i,j);
            //scanf("%f",&u[i][j]);
            u[i][j]=0;
        }
    }

    u[0][1] = sin(pi*0.25);
    u[0][2] = sin(pi*0.5);
    u[0][3] = sin(pi*.75);


    //Diagonal formula
    printf("Using diagonal formula\n");
    u[2][2] = formula(u[0][4],u[4][4],u[0][0],u[4][0]);

    printf("u[2][2] is %f\n",u[2][2]);
    u[1][1] = formula(u[0][2],u[2][2],u[0][0],u[2][0]);
    printf("u[1][1] is %f\n",u[1][1]);
    u[1][3] = formula(u[0][4],u[2][2],u[0][2],u[2][4]);
    printf("u[1][3] is %f\n",u[1][3]);
    u[3][3] = formula(u[2][4],u[2][2],u[4][2],u[4][4]);
    printf("u[3][3] is %f\n",u[3][3]);
    u[3][1] = formula(u[4][2],u[2][2],u[2][0],u[4][0]);
    printf("u[3][1] is %f\n",u[3][1]);

    //Standard formula
    printf("Using Standard formula\n");
    u[1][2] = formula(u[1][3],u[0][2],u[1][1],u[2][2]);
    printf("u[1][2] is %f\n",u[1][2]);
    u[2][1] = formula(u[2][2],u[1][1],u[2][0],u[3][1]);
    printf("u[2][1] is %f\n",u[2][1]);
    u[3][2] = formula(u[3][3],u[2][2],u[3][1],u[4][2]);
    printf("u[3][2] is %f\n",u[3][2]);
    u[2][3] = formula(u[2][4],u[1][3],u[2][2],u[3][3]);
    printf("u[2][3] is %f\n",u[2][3]);

    return 0;

}