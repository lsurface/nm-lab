#include<iostream>

using namespace std;

/*Fibonacci series for upto n terms */

int main(){

    int a=0,b=1,n,c;

    cout<<"Enter number of terms:\n";
    cin>>n;

    int count=3;
    cout<<a<<", "<<b;
    while(count<=n){
        cout<<", ";
        c=a+b;
        a=b;
        b=c;
        cout<<c;
        count++;
    }

    return 0;
}