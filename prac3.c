#include<stdio.h>
#include<math.h>

float F(float x, float y){
    return pow(x,2)+x*y-6;
}

float G(float x, float y){
    return pow(x,2)-pow(y,2)-3;
}

float Fx(float x, float y){
    return 2*x+y;
}

float Fy(float x, float y){
    return x;
}

float Gx(float x, float y){
    return 2*x;
}

float Gy(float x, float y){
    return -2*y;
}

float deltaX(float x, float y){
    float num = -F(x,y)*Gy(x,y) + G(x,y)*Fy(x,y);
    float den = Fx(x,y)*Gy(x,y) - Fy(x,y)*Gx(x,y);

    return num/den;
}

float deltaY(float x, float y){
    float num = -Fx(x,y)*G(x,y) + Gx(x,y)*F(x,y);
    float den = Fx(x,y)*Gy(x,y) - Fy(x,y)*Gx(x,y);

    return num/den;
}

int main(){

    int miter;
    float x0,y0,x1,y1;

    printf("Enter value for x: ");
    scanf("%f",&x0);

    printf("Enter value for y: ");
    scanf("%f",&y0);

    printf("Enter maximum number of iterations: ");
    scanf("%d",&miter);

    for(int i=0;i<miter;i++){

        x1 = x0 + deltaX(x0,y0);
        y1 = y0 + deltaY(x0,y0);

        printf("After %dth iteration, x = %f y = %f \n",i+1,x1,y1);

        x0 = x1;
        y0 = y1;

    }

    printf("Value for x is %f\n",x0);
    printf("Value for y is %f\n",y0);

    return 0;

}