#include<iostream>

using namespace std;

/*Check if a number is a prime number */

int main(){

    int n;
    bool prime=true;

    cout<<"Enter value for n\n";
    cin>>n;

    for(int i=2;i<=n/2;i++){
        if(n%i==0){
            prime=false;
            break;
        }
    }

    if(prime){
        cout<<n<<" is a prime number\n";
    }else{
        cout<<n<<" is not a prime number\n";
    }

    return 0;
}