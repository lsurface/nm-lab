#include<iostream>

using namespace std;

/*Display divisors of n */

int main(){

    int n;

    cout<<"Enter value for n\n";
    cin>>n;

    cout<<"Divisors for "<<n<<" are ";

    for(int i=1;i<=n;i++){
        
        if(n%i==0){
            cout<<i<<", ";
        }
    }
    return 0;
}