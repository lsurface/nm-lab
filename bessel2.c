#include<stdio.h>
#include<math.h>

//Factorial of n
int fact(int n){

    if(n==0||n==1){
        return 1;
    }else{
        return n*fact(n-1);
    }
}

//Finding the coefficient of central difference in Bessel's formula as from 3rd term
float besCoef(float p, int col){  

    float temp =p;
    for(int i=1;i<=col/2;i++){
        temp=temp*(p-1);
    }
    for(int i=1;i<col/2;i++){
        temp=temp*(p+1);
    }

    return temp;

}

int main(){

    int n;

    //Initializing the number of variables
    printf("Enter number of variables\n");
    scanf("%d",&n);

    float x[n];
    float y[n][n];

    //Step Difference
    float h;
    printf("Enter h value: ");
    scanf("%f",&h);

    printf("Enter value for x0: ");
    scanf("%f",&x[0]);

    for(int i=1;i<n;i++){
        x[i]=x[i-1]+h;
    }

    //Assigning x and y values
    for(int i=0;i<n;i++){
        
        printf("Enter value for y%d\n",i);
        scanf("%f",&y[i][0]);
    }




    //Calculating Central Difference
    for(int i=1;i<n;i++){
        for(int j=n-1;j>0;j--){
            y[j-1][i]=y[j][i-1]-y[j-1][i-1];
        }
    }

    //Displaying Central Difference table
    int k=n;
    //i is the row
    for(int i=0;i<n;i++){
        //k is the column
        for(int j=0;j<k;j++){
            printf("%f ",y[i][j]);
        }
        //To neglect the unnecessary values
        k--;
        printf("\n");
    }

    //Value of x to be found
    float besX;
    printf("Enter value for x: ");
    scanf("%f",&besX);

    //Index of the closest given value to the value to be found
    int index;
    printf("Enter index for x0 value: ");
    scanf("%d",&index);

    float p=(besX-x[index])/h;

    //First two terms of Bessel's formula
    float init = (y[index][0]+y[index+1][0])/2 + (p-0.5)*y[index][1];

    //Consideing the values from the 3rd column of the Central Difference table
    int col=2;

    //While the values are still from the zeroth row and above
    while(index>=0 && col<n){

        //Adding each term of the Bessel's Formula
        if(col%2==0){
            init = init+(besCoef(p,col)/fact(col))*((y[index][col]+y[index-1][col])/2);
            index--;
        }else{
            init = init+(besCoef(p,col)/fact(col))*(y[index][col]);
        }
        col++;
        
    }

    printf("Value at x=%f is %f\n",besX, init);
    return 0;
}