#include<iostream>
#include<math.h>

using namespace std;

/*Find value for f(x) for given x value for n terms */

int fact(int n){
    
    if(n==1){
        return 1;
    }else {
        return n*fact(n-1);
    }

    
}

float nterm(int n, float x){

    float i = (pow((-1),n+1)*pow(x,2*n-1))/fact(2*n-1);
    return i;
}

int main(){

    int n;
    float x,sum;

    cout<<"Enter value for x:\n";
    cin>>x;

    cout<<"Enter number of terms:\n";
    cin>>n;

    for(int i=1;i<=n;i++){
        
        sum=sum+nterm(i,x);
    }

    cout<<sum;

    return 0;
}