#include<stdio.h>
#include<math.h>

float F(float x, float y){
    return pow(x,3)+y-1;
}

float G(float x, float y){
    return -x+pow(y,3)+1;
}

float Fx(float x, float y){
    return 3*pow(x,2);
}

float Fy(float x, float y){
    return 1;
}

float Gx(float x, float y){
    return -1;
}

float Gy(float x, float y){
    return 3*pow(y,2);
}

float deltaX(float x, float y){
    float num = -F(x,y)*Gy(x,y) + Fy(x,y)*G(x,y);
    float den = Fx(x,y)*Gy(x,y) - Fy(x,y)*Gx(x,y);

    return num/den;
}

float deltaY(float x, float y){
    float num = -Fx(x,y)*G(x,y) + F(x,y)*Gx(x,y);
    float den = Fx(x,y)*Gy(x,y) - Fy(x,y)*Gx(x,y);

    return num/den;
}

int main(){

    int miter, iter=1;
    float x0,x1,y0,y1;

    printf("Enter initial value for x: ");
    scanf("%f",&x0);

    printf("Enter initial value for y: ");
    scanf("%f",&y0);

    printf("Enter number of iterations: ");
    scanf("%d",&miter);

    while(iter<=miter){
        x1=x0+deltaX(x0,y0);
        y1=y0+deltaY(x0,y0);

        printf("%dth iteration \n",iter);
        printf("x=%f, y=%f \n",x1,y1);

        x0=x1;
        y0=y1;

        iter++;

    }

    printf("After %d iterations, x is %f and y is %f \n",miter,x0,y0);

    return 0;
}