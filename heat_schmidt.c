#include<stdio.h>
#include<math.h>
#define pi 3.14159

float sm(float, float,float);
float alpha;

int main(){

    float a,b,c2,deltaX, deltaT;
    int imax,jmax;
    printf("Enter value of c^2:\n");
    scanf("%f",&c2);

    printf("Enter lower bound:\n");
    scanf("%f",&a);

    printf("Enter upper bound:\n");
    scanf("%f",&b);

    printf("Enter delta x:\n");
    scanf("%f",&deltaX);

    printf("Enter delta t:\n");
    scanf("%f",&deltaT);

    printf("Enter levels of t required:\n");
    scanf("%d",&jmax);

    alpha=(c2*deltaT)/pow(deltaX,2);

    imax=(b-a)/deltaX;

    float u[imax][jmax+1];

    for(int i=1;i<=jmax;i++){
        u[0][i]=0;
        u[imax][i]=0;
    }

    for(int i=0;i<imax;i++){
        u[i][0]=sin((a+i*deltaX)*pi);
    }

    for(int j=0;j<jmax;j++){
        for(int i=1;i<imax;i++){
            u[i][j+1]=sm(u[i-1][j],u[i][j],u[i+1][j]);
        }
    }

    for(int j=1;j<=jmax;j++){
        for(int i=1;i<imax;i++){
            printf("u[%d][%d] = %f\n",i,j,u[i][j]);
        }
    }

    return 0;
}

float sm(float u1,float u2,float u3){

    return alpha*u1+(1-2*alpha)*u2+alpha*u3;

}