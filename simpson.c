#include<stdio.h>
#include<math.h>

float F(float x, float y){

    return pow(x,2)+pow(y,2);
}

int main(){

    float h,k,a,b,c,d;
    int n,m;

    printf("Enter lower limit for x\n");
    scanf("%f",&a);

    printf("Enter upper limit for x\n");
    scanf("%f",&b);

    printf("Enter number of strips for x\n");
    scanf("%d",&n);

    h = (b-a)/(n);

    printf("Enter lower limit for y\n");
    scanf("%f",&c);

    printf("Enter upper limit for y\n");
    scanf("%f",&d);

    printf("Enter number of strips for y\n");
    scanf("%d",&m);

    k = (d-c)/(m);
    
    float x[n+1],y[m+1];

    x[0]=a;
    for(int i=1; i<=n;i++){
        x[i]=x[i-1]+h;
    }

    y[0]=c;
    for(int i=1;i<=m;i++){
        y[i]=y[i-1]+k;
    }

    float simp;

    simp = F(a,c) + F(a,d) + F(b,c) + F(b,d);

    for(int i=1;i<=m/2;i++){

        simp = simp + 4*F(a,y[2*i-1]) + 4*F(b,y[2*i-1]);

    }

    for(int i=1;i<=(m-1)/2;i++){

        simp = simp + 2*F(a,y[2*i]) + 2*F(b,y[2*i]);
    }

    for(int i=1;i<=n/2;i++){

        simp = simp + 4*F(x[2*i-1],c) + 4*F(x[2*i-1],d);
    }

    for(int i=1;i<=(n-1)/2;i++){

        simp = simp + 2*F(x[2*i],c) + 2*F(x[2*i],d);
    }

    for(int j=1;j<=m/2;j++){
        for(int i=1;i<=n/2;i++){
            simp = simp + 16*F(x[2*i-1],y[2*i-1]);
        }
    }

    for(int j=1;j<=(m-1)/2;j++){
        for(int i=1;i<=n/2;i++){
            simp = simp + 8*F(x[2*i-1],y[2*j]);
        }
    }

    for(int j=1;j<=m/2;j++){
        for(int i=1;i<=(n-1)/2;i++){
            simp = simp + 8*F(x[2*i],y[2*j-1]);
        }
    }

    for(int j=1;j<=(m-1)/2;j++){
        for(int i=1;i<=(n-1)/2;i++){
            simp = simp + 4*F(x[2*i],y[2*j]);
        }
    }

    simp = (h*k*simp)/9;

    printf("Value of Integral is %f\n",simp);
    return 0;
}