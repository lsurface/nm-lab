#include<iostream>

using namespace std;

/*Check if a number is a Palindrome */

int main(){

    int n,r, rev=0;

    cout<<"Enter value for n:\n";
    cin>>n;

    int d=n;

    while(n!=0){
        
        r=n%10;
        rev= rev*10+r;
        n=n/10;
    }

    if(rev==d){
        cout<<d<<" is a Palindrime\n";
    }else{
        cout<<d<<" is not a Palindrime\n";
    }

    return 0;
}