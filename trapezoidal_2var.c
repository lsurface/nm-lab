#include<stdio.h>
#include<math.h>

double F(double x,double y){

    return (8*exp(-pow(x,2)-pow(y,4)));
}

int main(){

    int n,m;

    double a,b,c,d,h,k;

    printf("Enter lower limit for x\n");
    scanf("%lf",&a);

    printf("Enter upper limit for x\n");
    scanf("%lf",&b);

    printf("Enter lower limit for y\n");
    scanf("%lf",&c);

    printf("Enter upper limit for y\n");
    scanf("%lf",&d);

    printf("Enter number of strips for x\n");
    scanf("%d",&n);

    printf("Enter number of strips for y\n");
    scanf("%d",&m);

    double x[n+1],y[m+1];

    h=(b-a)/n;
    k=(d-c)/m;

    x[0]=a;
    y[0]=c;

    for(int i=1;i<=n;i++){
        x[i]=x[i-1]+h;
    }

    for(int i=1;i<=m;i++){
        y[i]=y[i-1]+k;
    }

    double TR=F(a,c)+F(b,c)+F(a,d)+F(b,d);

    for(int i=1;i<n;i++){
        TR = TR + 2*(F(x[i],c)+F(x[i],d));
    }

    for(int i=1;i<m;i++){
        TR = TR + 2*(F(a,y[i])+F(b,y[i]));
    }

    for(int i=1;i<m;i++){
        for(int j=1;j<n;j++){
            TR = TR + 4*F(x[j],y[i]);
        }
    }

    TR = (TR*h*k)/4;

    printf("Value of integral is %lf\n",TR);

    return 0;
}