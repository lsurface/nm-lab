#include<iostream>

using namespace std;

/*Find out the prime factors of n */

int main(){
    int n;
    int i=2;

    cout<<"Enter value for n\n";
    cin>>n;

    while(i<=n){

        if(n%i==0){
            cout<<i<<"\n";
            n=n/i;
        }else{
            i++;
        }
    }
    return 0;
}