#include<stdio.h>
#include<math.h>

//Exponential function
double F1(float x){
    
    return (exp(pow(x,2))-1);
}

double F2(float x){
    return pow(cos(x),2);
}


int main(){

    int n;
    double a,b;
    printf("\nExponential Function\n");
    printf("Enter lower limit\n");
    scanf("%lf",&a);

    printf("Enter upper limit\n");
    scanf("%lf",&b);

    printf("Enter number of strips\n");
    scanf("%d",&n);

    double h=(b-a)/n;

    double x1[n+1],y1[n+1];

    x1[0]=a;

    for(int i=1;i<=n;i++){
        x1[i]=x1[i-1]+h;
        //printf("%lf\n",x1[i]);
    }

    for(int i=0;i<=n;i++){
        y1[i]=F1(x1[i]);
        //printf("%lf\n",y1[i]);
    }

    double TR1;


    TR1=y1[0]+y1[n];
    for(int i=1;i<n;i++){
        TR1=TR1+ 2*y1[i];
    }

    TR1=(h/2)*TR1;

    printf("Integral Value is %lf\n",TR1);

    printf("\nCos Function\n");
    printf("Enter lower limit\n");
    scanf("%lf",&a);

    printf("Enter upper limit\n");
    scanf("%lf",&b);

    printf("Enter number of strips\n");
    scanf("%d",&n);

    double x2[n+1],y2[n+1];

    h=(b-a)/n;

    x2[0]=a;

    for(int i=1;i<=n;i++){
        x2[i]=x2[i-1]+h;
    }

    for(int i=0;i<=n;i++){
        y2[i]=F2(x2[i]);
    }

    double TR2;

    TR2=y2[0]+y2[n];

    for(int i=1;i<n;i++){
        TR2=TR2+2*y2[i];
    }

    TR2=(h/2)*TR2;

    printf("Value of Integral is %lf\n",TR2);


    return 0;
 
}