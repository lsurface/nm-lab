#include<iostream>

using namespace std;

/*Check if n is a perfect number */

int main(){

    int n;
    int sum=0;

    cout<<"Enter value for n\n";
    cin>>n;

    for(int i=1;i<=n/2;i++){
        if(n%i==0){
            sum=sum+i;
        }
    }

    if(n==sum){
        cout<<n<<" is a perfect number\n";
    }else{
        cout<<n<<" is not a perfect number\n";
    }
    
    return 0;
}