#include<stdio.h>
#include<math.h>

float F(float x, float y){
    return x - pow(y,2);
}

int main(){

    float x0,y0,h,x;

    printf("Enter step size\n");
    scanf("%f",&h);

    printf("Enter initial value for x\n");
    scanf("%f",&x0);

    printf("Enter initial value for y\n");
    scanf("%f",&y0);

    printf("Enter value of x where y is to be found\n");
    scanf("%f",&x);

    
    while(x0 != x){

        float k1 = h*F(x0,y0);
        float k2 = h*F(x0+(h/2),y0+k1/2);
        float k3 = h*F(x0+(h/2),y0+k2/2);
        float k4 = h*F(x0+h,y0+k3);

        x0 = x0 + h;
        y0 = y0 + (k1+2*(k2+k3)+k4)/6;
    }
      

    printf("y(%f) is %f\n",x0,y0);

    return 0;
}