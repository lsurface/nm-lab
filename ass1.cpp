#include<iostream>
#include<assert.h>

using namespace std;

/*Determine grades w.r.t to marks */

int main(){
    int marks;
    

    cout<<"Enter marks: ";
    cin>>marks;
    assert(marks<=100);

    if(marks>=90){
        cout<<"A*\n";
    }else if(marks<90 && marks>=80){
        cout<<"A\n";
    }else if(marks<80 && marks>=70){
        cout<<"B\n";
    }else if(marks<70 && marks>=60){
        cout<<"C\n";
    }else if(marks<60 && marks>=50){
        cout<<"D\n";
    }else if(marks<50 && marks>=40){
        cout<<"E\n";
    }else{
        cout<<"Fail\n";
    }

    return 0;
    

}