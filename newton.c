#include<stdio.h>
#include<math.h>

double F(double x){
    return ((x)*(x)-5);
}

double Fd(double x){
    return (2*(x));
}

int main(){
    float x0,h,err,root,x1;
    int miter,iter;

    printf("Enter the first approximation, the max error and the maximum number of iterations\n");
    scanf("%f %f %d",&x0,&err,&miter);

    iter=1;

    while(iter<=miter){
        h=F(x0)/Fd(x0);
        x1=x0-h;
        printf("THe approximation's value after %d iteration is %f \n",iter,x1);
        if(fabs(h)<err){
            root=x1;
            break;
        }
        x0=x1;
        iter++;
    }
    if(root==x1){
        printf("The root is %f \n",root);
        double fncvalue =F(root);
        printf("Value of root is %f",fncvalue);
    }else{
        printf("Number of iteration is insufficient");
    }

    return 0;
}