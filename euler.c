#include<stdio.h>

float F(float x, float y){
    return (6-2*(y/x));
}

int main(){

    float x,y,h,xn;


    printf("Enter initial value for x\n");
    scanf("%f",&x);

    printf("Enter initial value for y\n");
    scanf("%f",&y);

    printf("Enter step size\n");
    scanf("%f",&h);

    printf("Enter x value for which y is to be found\n");
    scanf("%f",&xn);

    int k=(xn-x)/h;
    for(int i=0;i<k;i++){

        y=y+h*F(x+i*h,y);

    }
    printf("Value of y at x=%f is %f\n",xn,y);

    return 0;
}