#include<iostream>

using namespace std;

/*Find out the reverse of a number */

int main(){

    int n,r, rev=0;

    cout<<"Enter value for n:\n";
    cin>>n;

    while(n!=0){
        
        r=n%10;
        rev= rev*10+r;
        n=n/10;
    }

    cout<<"Reverse is "<<rev<<"\n";

    return 0;
}